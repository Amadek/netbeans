#ifndef CALC_H
#define CALC_H

class Calc
{
private:
    static bool ifExist;
public:
    Calc();
    ~Calc();
    int add(int a, int b);
    int power(int a, int n);
};

#endif /* HUMAN_H */

