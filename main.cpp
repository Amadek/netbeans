#include <iostream>
#include "Calc.h"
#include "Exception.h"
using namespace std;

int main()
{
    Calc * calc = new Calc();
    
    try
    {
        Calc calc2;
        cout << calc2.add(1, 2) << endl;
    }
    catch (CalcException e)
    {
        cout << "Nie można utworzyć drugiego obiektu klasy Calc()." << endl;
    }
    
    delete calc;
    return 0;
}