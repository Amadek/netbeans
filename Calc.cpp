#include "Calc.h"
#include "Exception.h"

bool Calc::ifExist = false;

Calc::Calc()
{
    if (!Calc::ifExist)
    {
        Calc::ifExist = true;
    }
    else
    {
        throw CalcException();
    }
}

Calc::~Calc()
{
    Calc::ifExist = false;
}

int Calc::add(int a, int b)
{
    return a + b;
}

int Calc::power(int a, int n)
{
    int s = a;
    while (n > 1)
    {
        s *= a;
        n--;
    }
    return s;
}
